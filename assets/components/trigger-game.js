/* global StartGameInstance, AFRAME */
class TriggerGame {
    constructor(GameManager, ShootEnemy, Score) {
        this.scene = document.querySelector('a-scene');
        this.Score = new Score();
        this.GameManager = new GameManager(this.scene, this.Score);
        this.ShootEnemy = new ShootEnemy(this.GameManager);
    }

    static hideStartBtn(el) {
        el.setAttribute("visible", 'false');
        el.setAttribute('position', '0 4 -4');
    }

    static showRestartBtn(el) {
        el.setAttribute('visible', 'true');
        el.setAttribute('position', '0 1 -4');
    }

    initiateGame() {
        this.GameManager.addEnemyToScene();

        for (let i = 0; i < 2; i++) {
            setTimeout(() => this.GameManager.addEnemyToScene(), 150);
        }
    }
}

AFRAME.registerComponent('start-game', {
    init: function () {
        const el = this.el;

        el.addEventListener('click', function () {
            StartGameInstance = new TriggerGame(GameManager, ShootEnemy, Score);
            TriggerGame.hideStartBtn(el);
            StartGameInstance.initiateGame();
        });
    }
});
AFRAME.registerComponent('restart-game', {
    init: function () {
        const el = this.el;

        el.addEventListener('click', function () {
            window.location.reload();
        });
    }
});
AFRAME.registerComponent('change-visuals', {
    init: function () {
        const el = this.el;

        if (AFRAME.utils.device.isMobile()) {
            el.addEventListener('loaded', function () {
                document.querySelector('#cursor').setAttribute('width', '0.3');
                document.querySelector('#cursor').setAttribute('height', '0.3');
            });

            el.addEventListener('enter-vr', function () {
                document.querySelector('#cursor').setAttribute('width', '0.125');
                document.querySelector('#cursor').setAttribute('height', '0.125');

                document.querySelector('#lifePoints').setAttribute('position', '0.5 -1 -1.9');
                document.querySelector('#score').setAttribute('position', '-0.5 -1 -1.9');

                document.querySelector('#gameOver').setAttribute('position', '0 2 -7');
            });

            el.addEventListener('exit-vr', function () {
                document.querySelector('#cursor').setAttribute('width', '0.3');
                document.querySelector('#cursor').setAttribute('height', '0.3');

                document.querySelector('#lifePoints').setAttribute('position', '2 -1 -1.9');
                document.querySelector('#score').setAttribute('position', '-2 -1 -1.9');

                document.querySelector('#gameOver').setAttribute('position', '0 3 -7');
            });
        }
    }
});