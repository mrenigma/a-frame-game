/* global AFRAME */
class Score {
    constructor () {
        this.total = document.querySelector('#score');
        this.GameManager = GameManager;
        this.hits = 0;
        this.score = 0;
    }

    addHit() {
        this.hits++;

        if (this.hits <= 3) {
            const heart = document.querySelector('#heart-' + this.hits);
            const mesh = heart.getObject3D('mesh');

            if (!mesh) {
                return;
            }

            mesh.traverse(function (node) {
                if (node.isMesh) {
                    node.material.opacity = 0.2;
                    node.material.transparent = true;
                    node.material.needsUpdate = true;
                }
            });
        }
    }

    addPoint() {
        this.score++;
        this.total.setAttribute('text', 'value', this.score);
    }
}