/* global StartGameInstance, AFRAME */
class ShootEnemy {
    constructor (GameManager) {
        this.GameManager = GameManager;
    }

    destroy(el) {
        this.GameManager.removeEnemy(el);
    }
}

AFRAME.registerComponent('shoot-enemy', {
    init: function () {
        const el = this.el;

        el.addEventListener('click', () => {
            console.log('shoot enemy: ' + el.parentNode.getAttribute('id'));
            StartGameInstance.ShootEnemy.destroy(el.parentNode);
            StartGameInstance.GameManager.addEnemyToScene();
            StartGameInstance.GameManager.addEnemyToScene();
        });
    }
});