class GameManager {
    constructor(scene, Score) {
        this.scene = scene;
        this.enemies = new Set();
        this.destroyedEnemies = new Set();
        this.Score = Score
    }

    static generateRandomNumber(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);

        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static generateRandomId() {
        let uuid = '';

        for (let i = 0; i < 32; i++) {
            const random = Math.random() * 16 | 0;

            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += "-"
            }
            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }

        return 'ufo-' + uuid;
    }

    static chooseRandomPosition() {
        const xPos = GameManager.generateRandomNumber(-10, 40);
        const yPos = GameManager.generateRandomNumber(2, 10);
        const zPos = GameManager.generateRandomNumber(-20, -40);

        return {'x': xPos, 'y': yPos, 'z': zPos};
    }

    createEnemy() {
        const newEnemyGroup = document.createElement('a-entity');
        const newEnemy = document.createElement('a-entity');
        const id = GameManager.generateRandomId();
        const position = GameManager.chooseRandomPosition();

        newEnemyGroup.setAttribute('id', id);
        newEnemy.setAttribute('shoot-enemy', '');
        newEnemy.setAttribute('class', 'raycasterClickable');
        newEnemy.setAttribute('raycaster-autorefresh', '');
        newEnemy.setAttribute('gltf-model', '#ufo');

        newEnemyGroup.setAttribute('position', position.x.toString() + ' ' + position.y.toString() + ' ' + position.z.toString());
        newEnemy.setAttribute('rotation', '-0.5 0 0');

        newEnemy.setAttribute('animation', {
            property: 'rotation',
            to: '0.5 360 0',
            loop: true,
            autoplay: true,
            dur: 1000
        });
        newEnemyGroup.setAttribute('animation', {
            property: 'position',
            to: '0 1.6 0',
            autoplay: true,
            dur: 20000
        });

        this.enemies.add(id);

        newEnemyGroup.appendChild(newEnemy);
        newEnemyGroup.addEventListener('animationcomplete', this.triggerEnemyHit.bind(this));

        return newEnemyGroup;
    }

    triggerEnemyHit(el) {
        const sky = document.querySelector('#sky');
        const enemy = el.target;

        if (!this.destroyedEnemies.has(enemy.getAttribute('id'))) {
            sky.setAttribute('src', '#skyTextureHit');

            this.removeEnemyFromScene(enemy);
            this.Score.addHit();

            if (this.Score.hits === 3) {
                this.showGameOver();
            } else {
                setTimeout(() => {
                    sky.setAttribute('src', '#skyTexture');
                }, 200);
            }
        }
    }
    addEnemyToScene() {
        this.scene.appendChild(this.createEnemy())
    }

    removeEnemyFromScene(el) {
        const id = el.getAttribute('id');

        this.destroyedEnemies.add(id);
        el.parentNode.removeChild(el);
        this.enemies.delete(id);
    }

    removeEnemy(el) {
        this.removeEnemyFromScene(el);
        this.Score.addPoint();
    }

    showGameOver() {
        document.querySelector('#gameOver').setAttribute('visible', 'true');
        TriggerGame.showRestartBtn(document.querySelector('#restartBtn'));

        this.enemies.forEach((id) => this.removeEnemyFromScene(document.querySelector('#' + id)));
    }
}