AFRAME.registerComponent('raycaster-autorefresh', {
    init: function () {
        const el = this.el;

        this.el.addEventListener('model-loaded', function () {
            const cursorEl = document.querySelector('#cursor');

            cursorEl.components.raycaster.refreshObjects();
        });
    }
});